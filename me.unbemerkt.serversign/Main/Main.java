package me.unbemerkt.serversign.Main;

import me.unbemerkt.serversign.LISTENER.SignEvents;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.*;

public class Main extends JavaPlugin implements PluginMessageListener {
    public static Main plugin;

    public static Main getInstance()
    {
        return instance;
    }

    public static void setInstance(Main instance)
    {
        Main.instance = instance;
    }

    public static String getPrefix()
    {
        return prefix;
    }

    public static String getNoperms()
    {
        return Noperms;
    }

    public SignEvents getSignEvents()
    {
        return new SignEvents();
    }

    private static String prefix = "§bServerSign| §7";
    public static String nperms = "§cServerSign | §4";
    private static String Noperms = nperms + "Dazu hast du keine Berechtigung!";
    private static Main instance;
    private PluginManager pm = Bukkit.getPluginManager();

    @Override
    public void onEnable(){
        plugin = this;
        setInstance(this);
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
        ConsoleCommandSender console = Bukkit.getConsoleSender();
        registerCommands();
        registerEvents();
        console.sendMessage("§bServerSign | §7Plugin Erfolgreich Aktiviert!");
        console.sendMessage("§bServerSign | §7Danke das Sie sich fuer ServerSign Entschieden Haben!");
    }

    public void onDisable()
    {
        ConsoleCommandSender console = Bukkit.getConsoleSender();
        console.sendMessage("§cWarp-System | §7Plugin Erfolgreich Deaktiviert!");
        console.sendMessage("§cWarp-System | §7Danke das Sie sich fuer Warp-System Entschieden Haben!");
    }

    public void registerCommands()
    {

    }
    public void registerEvents(){
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new SignEvents(), this);
    }



    public static String serverName; // Beispiel: GetServer Subchannel




    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }

        DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
        String subchannel = null;
        try {
            subchannel = in.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (subchannel.equals("SomeSubChannel")) {
            // Codebeispiel unten
        } else if (subchannel.equals("SomeOtherSubChannel")) {
            // Anderer Subchannel
        } else if (subchannel.equals("GetServer")) {
            // Beispiel: GetServer subchannel

            try {
                serverName = in.readUTF();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void send(Player p, String server){
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try{
            out.writeUTF("Connect");
            out.writeUTF(server);
        }catch(IOException e){

        }
        p.sendPluginMessage(Main.getInstance(), "BungeeCord", b.toByteArray());
    }

}



