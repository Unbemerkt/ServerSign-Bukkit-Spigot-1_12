package me.unbemerkt.serversign.LISTENER;



import me.unbemerkt.serversign.Main.Main;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SignEvents implements Listener {
    private Main plugin;
    ByteArrayOutputStream b = new ByteArrayOutputStream();
    DataOutputStream out = new DataOutputStream(b);


    @EventHandler
    public void SignChange(SignChangeEvent e){
        Player p = e.getPlayer();
        if (e.getLine(0).equalsIgnoreCase("[serversign]")) {
            if (p.hasPermission("serversign.create")){
                if (!e.getLine(1).equalsIgnoreCase("")) {
                    e.setLine(0, "~~~~~~~~~~~~");
                    e.setLine(1, e.getLine(1));
                    e.setLine(2, "§7>>§aJOIN§7<<");
                    e.setLine(3, "~~~~~~~~~~~~");
                } else {
                    e.setLine(0, "§c[ServerSign]");
                    e.setLine(1, "§cKein Ziel Gesetzt!");
                    p.sendMessage(Main.getPrefix() + "Du kannst kein ServerSign Schild ohne Ziel erstellen!");
                }
            }
        }else{

        }
    }


    @EventHandler
    public void onPlayerInteractWithSign(PlayerInteractEvent e){
        Player p = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK){
            if (e.getClickedBlock().getState() instanceof Sign){
                Sign s = (Sign) e.getClickedBlock().getState();
                if (s.getLine(0).equalsIgnoreCase("~~~~~~~~~~~~") && s.getLine(3).equalsIgnoreCase("~~~~~~~~~~~~")&& s.getLine(2).equalsIgnoreCase("§7>>§aJOIN§7<<")){
                        if (!s.getLine(1).equalsIgnoreCase("")) {
                            Main.getInstance().send(p,s.getLine(1));
                        }


                }
            }
        }
    }
}
